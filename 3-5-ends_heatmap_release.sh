### Script to generate the heatmap displaying piRNA 3/5ends around abundant ping-pong pairs

#-------------------------------------------------------------------------------------------------------
#presetting of variables 
NAMEforANALYSIS=

#Folder of files to keep - use for other analysis
rawFOLDER=

#output folder
rawOUTPUT=

#norm is to normalise sequencing depth to 1 mil miRNA reads
norm=

#Cores avaiable for computing
NSLOTS=

#bowtie index for TE mapping
TE_index=

#fasta file for TE consensus sequences
TE_fasta=

#-region to +region will be the window of heatmap
region=

#R code to make the heatmap and output tables for the downstream analysis
RSCRIPT=


#-------------------------------------------------------------------------------------------------------
#prepare all vairables
FOLDER=${rawFOLDER}/${NAMEforANALYSIS}/${NAMEforANALYSIS}
OUTPUT=${rawOUTPUT}/${NAMEforANALYSIS}/${NAMEforANALYSIS}
#-------------------------------------------------------------------------------------------------------

#create all folders
mkdir -p ${rawOUTPUT}
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}

echo "start"
echo ${NAMEforANALYSIS}
echo ${norm}


### map the TE reads using curated_collapsed.bed from /end_analysis/end_analysis_length.sh --- BEGIN ---

### bowtie allowing three MM (-v 3) and --best and --strata to exclude mappings when there are better mappers. -a is to take all mappers, which overrules --best and --strata.
awk '{split($4,a,"_"); split(a[2],b,"?"); print ">"a[1]"@"$5"\n"b[1]}' ${FOLDER}_curated_collapsed.bed |\
bowtie -f -v 3 -a --best --strata -S -p $NSLOTS ${TE_index} - |\
samtools view -bS - | \
bamToBed -i - |\
awk -v OFS="\t" -v norm=$norm '{ split ($4,split_name,"@"); $5=split_name[2]; print $1,$2,$3,$4,$5/norm*1000,$6 }' > ${OUTPUT}_TE_3MM_collapsed_normalised.bed

### map the TE reads using curated_collapsed.bed from /end_analysis/end_analysis_length.sh --- END ---



### generate bedgraph using homer --- BEGIN ---

#define fragment length for bedgraph and the name extension
fragLength="1"

### for the 5'ends
makeTagDirectory ${OUTPUT}-tagdir_5end ${OUTPUT}_TE_3MM_collapsed_normalised.bed -single -format bed -force5th

makeUCSCfile ${OUTPUT}-tagdir_5end -fragLength ${fragLength} -fsize 5e20 -strand + -noadj -normLength 0  | \
  awk -v OFS="\t" '{if(NR>1) print $1,$2,$3,$4 }'  > ${OUTPUT}_TE_3MM_collapsed_normalised_+_5end.bedgraph
makeUCSCfile ${OUTPUT}-tagdir_5end -fragLength ${fragLength} -fsize 5e20 -strand - -noadj -normLength 0  | \
  awk -v OFS="\t" '{if(NR>1) print $1,$2,$3,$4 }'  > ${OUTPUT}_TE_3MM_collapsed_normalised_-_5end.bedgraph

### for the 3'ends
awk -v OFS="\t" '{ if($6=="+") {$2=$3-1; print} else {$3=$2+1; print}  }' ${OUTPUT}_TE_3MM_collapsed_normalised.bed > ${OUTPUT}_TE_3MM_collapsed_normalised_3end.bed

makeTagDirectory ${OUTPUT}-tagdir_3end ${OUTPUT}_TE_3MM_collapsed_normalised_3end.bed -single -format bed -force5th

makeUCSCfile ${OUTPUT}-tagdir_3end -fragLength ${fragLength} -fsize 5e20 -strand + -noadj -normLength 0  | \
  awk -v OFS="\t" '{if(NR>1) print $1,$2,$3,$4 }'  > ${OUTPUT}_TE_3MM_collapsed_normalised_+_3end.bedgraph
makeUCSCfile ${OUTPUT}-tagdir_3end -fragLength ${fragLength} -fsize 5e20 -strand - -noadj -normLength 0  | \
  awk -v OFS="\t" '{if(NR>1) print $1,$2,$3,$4 }'  > ${OUTPUT}_TE_3MM_collapsed_normalised_-_3end.bedgraph

### generate bedgraph using homer --- END ---



### select abundantly expressed ping-pong pairs --- BEGIN ---

#determine the maximum value per TE
sort -k1,1 -k4,4rn ${OUTPUT}_TE_3MM_collapsed_normalised_+_5end.bedgraph | awk '{print $4,$1 }' | uniq -f 1 | awk '{print $2,$1 }'  > ${OUTPUT}_TE_3MM_collapsed_ping-pong_+_max-per-TE.txt
sort -k1,1 -k4,4rn ${OUTPUT}_TE_3MM_collapsed_normalised_-_5end.bedgraph | awk '{print $4,$1 }' | uniq -f 1 | awk '{print $2,$1 }'  > ${OUTPUT}_TE_3MM_collapsed_ping-pong_-_max-per-TE.txt


#create bedgraph index 
fasta_formatter -i $TE_fasta -t | awk '{ for (i=1; i<=length($2); i++) {print $1,$1"~"i"~"i+1 }}' |sort -k1,1 |\
join -a 1 - ${OUTPUT}_TE_3MM_collapsed_ping-pong_+_max-per-TE.txt | \
join -a 1 - ${OUTPUT}_TE_3MM_collapsed_ping-pong_-_max-per-TE.txt | tr ' ' '\t' |  cut --complement -f 1 | sort -k1,1 > ${OUTPUT}_TE_3MM_collapsed_bedgraph_index.txt
### ${OUTPUT}_TE_3MM_collapsed_bedgraph_index.txt looks as follows:
#<TE> <TE ~ position(from 1 to the length of TE) ~ position+1> <max-per-TE>


#create a combined file containing the sense and antisense bedgraphs
join -a 1 -e 0 -o auto ${OUTPUT}_TE_3MM_collapsed_bedgraph_index.txt <(awk '{print $1"~"$2"~"$3,$4}' ${OUTPUT}_TE_3MM_collapsed_normalised_+_5end.bedgraph | sort -k1,1 ) |\
join -a 1 -e 0 -o auto - <(awk '{ if($2>=10) {print $1"~"$2-9"~"$3-9,$4 }}' ${OUTPUT}_TE_3MM_collapsed_normalised_-_5end.bedgraph | sort -k1,1 ) | \
join -a 1 -e 0 -o auto - <(awk '{print $1"~"$2"~"$3,$4}' ${OUTPUT}_TE_3MM_collapsed_normalised_-_5end.bedgraph | sort -k1,1 ) |\
tr '~' ' ' | sort -k1,1 -k2,2n |\
awk 'BEGIN {print "chr","start","stop","max_+","max_-","value_+","value_-_10bp-off","value_-"} {print $0} ' | tr ' ' '\t'  > ${OUTPUT}_TE_3MM_combined.bedgraph


### call the coordinates of pingpong 5ends from sense and antisense with certain thresholds
### Cutoffs: both pairs have more than 10 reads / 1mil miRNA reads ($6>10000 && $7>10000) and both pairs have more than 2% of the maximum read per TE and per strand ($6>$4/50 && $7>$5/50).
awk '(NR>1 && $6>$4/50 && $7>$5/50 && $6>10000 && $7>10000) {print $1,$2,$3,$1"_"$2,$6"_"$8,"+"}' ${OUTPUT}_TE_3MM_combined.bedgraph > ${OUTPUT}_TE_3MM_peaks.bed 

### select abundantly expressed ping-pong pairs --- END ---



### for each ping-pong 5end, find the dominant length and its cloning count. These values will be used for later sorting. --- BEGIN ---

awk -v OFS="\t" -v strand="+" '{ $4=$4"_"strand; print $1,$2-1,$3-1,$4,$5,"+"}' ${OUTPUT}_TE_3MM_peaks.bed | sort -k1,1 >  ${OUTPUT}_TE_3MM_+_peaks.bed
awk -v OFS="\t" -v strand="-" '{ $4=$4"_"strand; print $1,$2+9,$3+9,$4,$5,"-" }' ${OUTPUT}_TE_3MM_peaks.bed | sort -k1,1 >  ${OUTPUT}_TE_3MM_-_peaks.bed

for strand in + -;  do
echo "TE max_value pos peak_value" | tr ' ' '\t' > ${OUTPUT}_TE_3MM_length_index_${strand}.txt

  awk -v PEAKfile="${OUTPUT}_TE_3MM_${strand}_peaks.bed" -v strand=$strand '
    BEGIN{
      MAXvalue=0
      while((getline PEAKlines < PEAKfile) > 0) {  split(PEAKlines,rawPEAK,/\t/); PEAKS[rawPEAK[1]][rawPEAK[2]]=rawPEAK[4] }
    }
    { 
      #convert 0-based bed into coordinates according 1-based peak-file
      Y=$3-$2
      if(strand == "+") {
        if($6==strand) {
          $2=$2-1
          X[$1][$2][Y]+=$5 
        }
      }
      else {
        if($6==strand) {
          $3=$3-1
          X[$1][$3][Y]+=$5 
        }
      }
        
    } 
    END{ 
      for (TE in PEAKS) { 
        for (STARTpos in PEAKS[TE]) {
          # print TE,STARTpos
          for(POSITION in X[TE][STARTpos]) {
            
            if( X[TE][STARTpos][POSITION] > MAXvalue) {
              MAXvalue=X[TE][STARTpos][POSITION]
              MAXpos=POSITION
            }
          }
          print PEAKS[TE][STARTpos],MAXvalue,MAXpos,MAXvalue
          MAXvalue=0
          MAXpos=""

        }        
      }
    }' ${OUTPUT}_TE_3MM_collapsed_normalised.bed |  tr ' ' '\t' | sort -k1,1 >> ${OUTPUT}_TE_3MM_length_index_${strand}.txt
done

### ${OUTPUT}_TE_3MM_length_index_${strand}.txt looks as follows:
#<TE _ 5end position> <cloning count of dominant length> <dominant length> <cloning count of dominant length>

### for each ping-pong 5end, find the dominant length and its cloning count. These values will be used for later sorting. --- END ---



### extract counts of reads mapping to the given windown flanking the pingpong 5ends --- BEGIN ---
last_col=$(expr $region \* 2 + 2 )

for strand in + -; do
for ends in 5end 3end; do
#extract data-tables from bedgraphs for peaks in the peak file
annotatePeaks.pl ${OUTPUT}_TE_3MM_${strand}_peaks.bed $TE_fasta -size -${region},${region} -len 1 -hist 1 -noadj -normLength 0 -ghist -bedGraph ${OUTPUT}_TE_3MM_collapsed_normalised_${strand}_${ends}.bedgraph |\
awk '{ if (NR>1) print }' | cut -f -$last_col > ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}_tmp.txt 
    
#generate the header for the output table
ids=$( seq -${region} ${region} | awk '{ print "pos_"$1 }' | tr '\n' '\t' )
printf "TE\tping_pong_count\t$ids\n" > ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}_tmp2.txt

#add value 0 after after the pingpong-5end-position for later graphication
sort -k1,1 ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}_tmp.txt | \
awk -v OFS="\t" '{ $1=$1"\t"0; print }'  >> ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}_tmp2.txt

join ${OUTPUT}_TE_3MM_length_index_${strand}.txt ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}_tmp2.txt > ${OUTPUT}_TE_3MM_collapsed_heat_${strand}_${ends}.txt
    
done
done
### extract counts of reads mapping to the given windown flanking the pingpong 5ends --- END ---


### R script to generate TE_3MM_percent_3end.jpg, TE_3MM_percent_5end.jpg, TE_3MM_percent_3end_sum.txt, and TE_3MM_percent_5end_sum.txt
Rscript ${RSCRIPT} output=${OUTPUT} NAMEforANALYSIS=${NAMEforANALYSIS} region=$region
### TE_3MM_percent_3end_sum.txt looks as follows:
#<sum of probabilities from all entries at position -region> <sum of probabilities from all entries at position -region+1> ..... <sum of probabilities from all entries at position region>


#merge heatmaps for 3end and 5end
composite -blend 50 ${OUTPUT}_TE_3MM_percent_5end.jpg ${OUTPUT}_TE_3MM_percent_3end.jpg ${OUTPUT}_TE_3MM_heatmap_percent_merged.jpg
